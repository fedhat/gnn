# GNN.jl Documentation

```@meta
CurrentModule = GNN
DocTestSetup = quote
  using GNN
end
```

```@autodocs
Modules = [GNN]
Order   = [:function, :type]
```
