# add the files of our library to the path seen by julia
push!(LOAD_PATH, "../src/")
println(LOAD_PATH)

using Documenter
using GNN
# using Plots

# title of the documentation
makedocs(sitename="GNN.jl",
         modules=[GNN],
         doctest=false
         # clean=true
)

# TODO
# deploydocs(;
#     repo="github.com/MineralsCloud/EquationsOfState.jl",
# )
