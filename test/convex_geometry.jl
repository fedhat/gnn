"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "prox" begin
    W = [-3 -2 -1; 0 1 2 ; 3 4 5]
    k = 1
    V = soft_thresholding(W, k)
    @test V == [-2 -1 0; 0 0 1; 2 3 4]
end

@testset "norm" begin
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = eye(G)
    norm(Wgg, (1,1))

end

@testset "nuclear norm prox" begin
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = eye(G)
    # println(Wgg)
    k = 0.5
    # alphas = ones(length(G))
    Pgg = nuclear_norm_prox(Wgg, k, G)
    @test Pgg == [[0.5 0.0 0.0; 0.0 0.5 0.0], [0.5 0.0; 0.0 0.5]]

    snorm(Wgg) = 4.0
end

@testset "LMO" begin
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = ones(G);
    (u, v, i) = LMO_snorm_1(Wgg, G)
end

@testset "rank" begin
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = eye(G)
    @test rank(Wgg) == [2, 2]
    Vgg = zeros(G)
    Vgg[1][1] = 1
    Vgg[2][1] = 1
    @test rank(Vgg) == [1, 1]

end
