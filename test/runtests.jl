"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

using GNN
using Test
using Arpack, LinearAlgebra, Random, Statistics

tests = ["algebraic_structure", "convex_geometry", "fista",
         "linear_functions", "synth_data", "utilities", "verification"]

for t in tests
    @testset "$t" begin
        include("$(t).jl")
    end
end
