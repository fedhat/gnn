"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""


@testset "part 1 test utilities" begin
    @test 1 + 3 == 4
    @test 1 + 3 == 4
    @test 1 + 3 == 4
    @test 1 + 3 == 4
    @test debug_soft() == true
end

@testset "group creation" begin
    d = 4 # rows
    k = 6 # columns
    width = 3
    height = 2
    rng = MersenneTwister(45)

    sa_G = Group([])
    append_small_blocks!(sa_G, 5, d, k, width, height, rng)
    sa_L = fill_uniform(sa_G, d, k, rng=rng)

    a_G = Group([])
    append_group_crossed!(a_G, d, k)
    a_L = fill_uniform(a_G, d, k, rng=rng)

    L = a_L + sa_L
    G = [a_G; sa_G]
    @test sizes(G) == [(3, 4), (1, 6), (4, 2), (2, 3), (2, 3), (2, 3), (2, 3), (2, 3), (2, 3)]

end
