"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "Group" begin
    block = Block([1,3], [1,3,4]);
    @test block.r == [1, 3]
    @test block.c == [1, 3, 4]
    group = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    @test sizes(group) == [(2, 3), (2, 2)]
end

@testset "standard linear algebra" begin
    M = eye(4)
    @test snorm(M, p=1) == 4.0
    @test snorm(M, p=2) == 2.0
    @test snorm(M, p=Inf) == 1.0
end

@testset "Mondrian linear algebra" begin
    # using LinearAlgebra
    group = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = zeros(group)
    @test size(Wgg) == (2,)
    @test sizes(Wgg) == [(2, 3), (2, 2)]
    Wgg = eye(group)
    Wgg = ones(group)
    @test squared_frobenius(Wgg) == 10.0
    @test size(Wgg) == (2,)
    @test sizes(Wgg) == [(2, 3), (2, 2)]
    @test norm(Wgg, (1, 1)) == 10.0
    (Ugg, Sgg, Vtgg) = svd(Wgg)
    @test norm(Ugg*Sgg*Vtgg - Wgg) < 1e-10
    (Ugg1, Sgg1, Vtgg1) = svds(Wgg, nsv=1)
    @test norm(Ugg1*Sgg1*Vtgg1 -Wgg ) < 1e-10 #ok only because Wgg has rank 1
    M = assemble(Wgg, (5,4), group)
    @test convert(Array{Int}, M) == [1 1 2 1; 0 0 0 0; 1 0 1 1; 0 0 0 0; 0 1 1 0]
end

@testset "operations" begin
    E = Mondrian([[1 2; 3 4], [1 2 3; 5 6 9; 5 2 8]])
    F = Mondrian([[1 0; 0 4], [1 0 3; 0 6 9; 0 2 8]])
    @test E ⊙ F ==  [[1.0 8.0; 3.0 16.0], [1.0 18.0 45.0; 5.0 54.0 141.0; 5.0 28.0 97.0]]
end

@testset "checks" begin
    group = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    Wgg = zeros(group);
    @test is_compatible(Wgg, group)
    group2 = Group([Block([1,3], [1,3,4])])
    @test !is_compatible(Wgg, group2)
end

@testset "full space" begin
    group = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    M = eye(5,4)
    @test norm(proj_on_small_space(M, group, 1), 1) == 2.0
end
