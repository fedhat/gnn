"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "fill uniform" begin
    G = Group([Block(1:5, 1:3), Block(1:5,[4,5])])
    d = 5
    k = 6
    rng = MersenneTwister(33)
    fill_uniform(G, d, k, rng=rng)
    fill_full_rk1(d, k)
    fill_affine(G, d, k, rng=rng)
end
