"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "groups check" begin
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
    (r1, r2) = is_disjoint(G, (10,10)) #-> false, false
    @test !r1
    @test !r2

    G = Group([Block(1:5, 1:3), Block(1:5,[4,5])]);
    (r3, r4) = is_disjoint(G, (5,5)) # -> true, true
    @test r3
    @test r4
end
