"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "Projections" begin
    rng = MersenneTwister(42)
    G = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])])
    local W
    for i in 1:length(G)
        W_i = randn(rng, size(G[i]))
        W = immersion_in_big_space(W_i, G, i, (6, 6))
        @test norm(W) - norm(W_i) <= 1e-5
        V_i = proj_on_small_space(W, G, i)
        @test norm(W_i - V_i) <= 1e-5

    end
    Vgg = op_adjoint(W, G)
    @test norm(Vgg -[[0.0 -2.641991008076796 0.0; 0.0 0.0 0.0], [-0.46860588216767457 -2.641991008076796; 0.15614346264074028 1.0033099014594844]]) < 1e-8

end

@testset "mask_idx_adj" begin
    idx = [1 4 5 9 12]
    x =   [1 1 1 1 1]
    dims = (3,4)
    @test mask_idx_adj(x, idx, dims) ==  [1.0 1.0 0.0 0.0; 0.0 1.0 0.0 0.0; 0.0 0.0 1.0 1.0]
end
