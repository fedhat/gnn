"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

@testset "fista" begin

    G = Group([Block(1:5, 1:3), Block(1:5,[4,5])])
    Wgg_0 = zeros(G)

    reg_0 = snorm(Wgg_0, p=(1.0,1.0))

end
