"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""


function append_small_blocks!(G::Group, num_groups::Number, d::Number, k::Number, width::Number, height::Number, rng)

    for i in 1:num_groups
        @assert width <= k
        @assert height <= d
        y1 = 1 + floor(rand(rng, 1)[1] * (d - height))
        y2 = y1 + height - 1;
        x1 = 1 + floor.(rand(rng, 1)[1] * (k - width))
        x2 = x1 + width - 1;
        new_block = Block(y1:y2, x1:x2)
        push!(G, new_block)
    end
end


function append_group_crossed!(G, d, k)
    r1 = floor(0.55 * d)
    r2 = floor(0.75 * d)
    c1 = floor(0.55 * k)
    c2 = floor(0.75 * k)
    G_new = Group([Block(1:r2,1:c2),
                   Block((r2+1):d,1:k),
                   Block(1:d, (c2+1):k),
                   Block((r1+1):d, (c1+1):k)
                   ])
    append!(G, G_new)
end


function plot_soft_thresholding()
    x = collect(-3:0.1:3)
    τ = 1
    y = soft_thresholding(x, τ)
    plot(x, y, legend=false, m=:star)
    return
end

function debug_soft()
    X = [-1 0 1; -2 1 0; 0 1 2]
    τ = 1
    Y = [0 0 0; -1 0 0; 0 0 1]
    Z = soft_thresholding(X, τ)
    return Y == Z
end

println("done utilities")
