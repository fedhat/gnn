"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""
# __precompile__()

"""
    GNN

Module on Group Nuclear Norm
"""
module GNN

using Arpack, LinearAlgebra, Random, Statistics
import Base: size, show, zeros, ones, copy, *


export Group, Block, Mondrian
export ⊙, hadamard, *, eye, sizes, max_size
include("algebraic_structure.jl")


export append_group_crossed!, append_small_blocks!, debug_soft,
       plot_soft_thresholding
include("utilities.jl")


export assemble, proj_on_small_space, immersion_in_big_space,
       op_adjoint, mask_idx_adj
include("linear_functions.jl")


export is_disjoint, is_compatible
include("verification.jl")


export LMO_snorm_1, nuclear_norm_prox, rank, snorm, soft_thresholding,
       squared_frobenius, svd, svds
include("convex_geometry.jl")


export append_sparse_blocks!, fill_affine, fill_uniform, fill_full_rk1,
       fill_small_rectangles_flat
include("synth_data.jl")


export fista, launch_fista
include("fista.jl")


end
