"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

"""
Here we define the space where the elements are block-structured matrices.
We define also a type for the indices of the blocks
"""

"""
    Block

```jldoctest
julia> bloc = Block([1,3], [1,3,4]);

julia> print(block)
row indices: [1, 3]
col indices: [1, 3, 4]

julia> print(block)
[1, 3][1, 3, 4]
```

"""
struct Block
    r :: Array{Int} #row indices
    c :: Array{Int} #col indices
end

function Base.size(b::Block)
    return (length(b.r), length(b.c))
end

function Base.show(io::IO, b::Block)
    println(io, "$(b.r)×$(b.c)")
end

"""
    Group

```jldoctest
julia> bloc1 = Block([1,3], [1,3,4])
[1, 3]×[1, 3, 4]

julia> bloc2 = Block([1,5], [2,3])
[1, 5]×[2, 3]

julia> g = Group([bloc1, bloc2])
2-element Array{Block,1}:
 [1, 3]×[1, 3, 4]
 [1, 5]×[2, 3]

julia> g2 = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);

julia> print(Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]))
Block[[1, 3]×[1, 3, 4], [1, 5]×[2, 3]]
```
"""
Group = Array{Block}

function sizes(g::Group)
    out = []
    for b in g
        push!(out, size(b))
    end
    return out
end


"""
    max_size(G::Group)

Find the largest row and largest column in the group `G`.
A block of size `(row, col)` does not necessairily exist.

Return
- `(row, col)`
"""
function max_size(G::Group)
    row = 0
    col = 0
    for b in G
        row = max(row, maximum(b.r))
        col = max(col, maximum(b.c))
    end
    return (row, col)
end

"""
    Mondrian

Space whose elements are lists of matrices.
The notation is the same as in the paper.

```jldoctest
julia> Wgg = Mondrian([[1 2; 3 4], [1 2 3; 5 6 9; 5 2 8]])
2-element Array{Array{Float64,N} where N,1}:
 [1.0 2.0; 3.0 4.0]
 [1.0 2.0 8.0; 5.0 6.0 9.0]
```
"""
Mondrian = Vector{Matrix{Float64}}

"""
    Base.Multimedia.display(Wgg::Mondrian)
"""
function Base.Multimedia.display(Wgg::Mondrian)
    for W in Wgg
        display(W)
    end
end

"""
    zeros(g::Group)

```jldoctest
julia> group = Group([Block([1,3], [1,3,4]), Block([1,5], [2,3])]);
julia> zeros(group)
2-element Array{Array{Float64,2},1}:
 [0.0 0.0 0.0; 0.0 0.0 0.0]
 [0.0 0.0; 0.0 0.0]
```
"""
function Base.zeros(group::Group)
    out = Mondrian()
    for block in group
        m = Base.zeros(Base.size(block))
        push!(out, m)
    end
    return out
end

"""
    eye(g::Group)

Mondrian object made with identity matrices
"""
function eye(g::Group)
    out = Mondrian()
    for b in g
        W = Matrix(I, length(b.r), length(b.c))
        push!(out, W)
    end
    return out
end

"""
    eye(rows::Int, cols::Int)

Identity matrix of shape `(rows, cols)`
"""
function eye(rows::Int, cols::Int)
    return Matrix(I, rows, cols)
end

"""
    eye(n::Int)

Identity matrix of shape `(n, n)`
"""
function eye(n::Int)
    return Matrix(I, n, n)
end

"""
    sizes(Wgg::Mondrian)

Return
    list of all sizes of the blocs

"""
function sizes(Wgg::Mondrian)
    out = Array{Tuple{Int64, Int64},1}()
    for m in Wgg
        push!(out, Base.size(m))
    end
    return out
end

# Construction methods
"""
    zeros(Wgg::Mondrian)

Make a zero element of the same shape of the given one
"""
function Base.zeros(Wgg::Mondrian)
    out = Mondrian()
    for s in sizes(Wgg)
        m = Base.zeros(s)
        push!(out, m)
    end
    return out
end

"""
    ones(Wgg::Mondrian)

Make a zero element of the same shape of the given one
"""
function Base.ones(Wgg::Mondrian)
    out = Mondrian()
    for s in sizes(Wgg)
        m = ones(s)
        push!(out, m)
    end
    return out
end

function Base.ones(group::Group)
    out = Mondrian()
    for block in group
        m = Base.ones(Base.size(block))
        push!(out, m)
    end
    return out
end

"""
    copy(Wgg::Mondrian)

We consider the element as a full entity, so we want to be sure that all submatrices are duplicated also
"""
function Base.copy(Wgg::Mondrian)
    return deepcopy(Wgg)
end

"""
    hadamard(Wgg::Mondrian, VGG::Mondrian)

Element-wise product between Mondrian objects
"""
function hadamard(Wgg::Mondrian, Vgg::Mondrian)
    Zgg = Mondrian()
    for (m, n) in zip(Wgg, Vgg)
        push!(Zgg, m*n)
    end
    return Zgg
end


"""
    ⊙

Elementwise product.
Same as `hadamard` function

```jldoctest
julia> Wgg = Mondrian([[1 2; 3 4], [1 2 3; 5 6 9; 5 2 8]]);
julia> Vgg = Mondrian([[1 0; 0 4], [1 0 3; 0 6 9; 0 2 8]]);

julia> Wgg ⊙ Vgg
2-element Array{Array{Float64,2},1}:
 [1.0 8.0; 3.0 16.0]
 [1.0 18.0 45.0; 5.0 54.0 141.0; 5.0 28.0 97.0]
```
"""
⊙(a::Mondrian, b::Mondrian) = hadamard(a, b)
# \odot


## operators overloading

"""
    Base.:*

Extension of matrix multiplication
"""
function Base.:*(Wgg::Mondrian, Vgg::Mondrian)
    return Mondrian([W*V for (W, V) in zip(Wgg, Vgg)])
end



println("done algebraic_structure")
