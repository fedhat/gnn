"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""


"""
    assemble(Wgg::Mondrian, dims::Tuple{Int, Int}, group::Group)

Projects all the blocks of `Wgg` into a single matrix if size `dims`
Equivalent to `immersion_in_big_space` applied to all blocks.

Return a matrix of size `dims = (rows, cols)`
"""
function assemble(Wgg::Mondrian, dims::Tuple{Int, Int}, group::Group)
    @assert length(Wgg) == length(group)
    rows, cols = dims
    M = zeros(rows, cols)
    for (W, b) in zip(Wgg, group)
        M[b.r, b.c] += W
    end
    return M
end

"""
    proj_on_small_space(W::Matrix, G::Group, i::Int)

Select in the full matrix `W` only the entries in the block `G[i]`.
Inverse of `immersion_in_big_space`
"""
function proj_on_small_space(W::Matrix, G::Group, i::Int)
    block = G[i]
    return W[block.r, block.c]
end

"""
    immersion_in_big_space(Wg ::Matrix, G::Group, i::Int, dims::Tuple{Int, Int})

Inverse of `proj_on_small_space`.
"""
function immersion_in_big_space(Wg ::Matrix, G::Group, i::Int, dims::Tuple{Int, Int})
    rows, cols = dims
    R = zeros(rows, cols)
    block = G[i]
    R[block.r, block.c] = Wg
    return R
end

"""
    op_adjoint(W::Matrix, G::Group)

Adjoint operator of `assemble`, s.t  `W = assemble(Wgg)`
"""
function op_adjoint(W::Matrix, G::Group)
    Wgg = Mondrian()
    for b in G
        push!(Wgg, W[b.r, b.c]) # == proj_on_small_space
    end
    return Wgg
end


function mask_idx_adj(x, idx, dims)
    W = zeros(dims)
    W[idx] = x
    return W
end

println("done linear_functions")
