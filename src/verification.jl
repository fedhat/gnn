"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

import Base

function is_disjoint(G::Group, dims::Tuple{Int, Int})
    T = Base.zeros(dims)

    for (i, b) in enumerate(G)
        w_g = Base.ones(size(b))
        W_g = immersion_in_big_space(w_g, G, i, dims)
        T += W_g
    end
    G_is_disjoint = sum(T.>=2) == 0
    G_covers_full_space = sum(T.==0) == 0
    return (G_is_disjoint, G_covers_full_space)
end


"""
    is_compatible(Wgg::Mondrian, g::Group)

Check whether the sizes of blocks in `Wgg` are the same as the sizes of blocks
in the group `g`
"""
function is_compatible(Wgg::Mondrian, g::Group)
    if length(Wgg) != length(g)
        return false
    else
        return sizes(Wgg) == sizes(g)
    end
end


println("done verification")
