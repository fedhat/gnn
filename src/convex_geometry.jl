"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""


"""
    nuclear_norm_prox(M::Matrix, k::Number)

Proximal operator of the nuclear norm
"""
function nuclear_norm_prox(M::Matrix, k::Number)
    F = LinearAlgebra.svd(M)
    d = soft_thresholding(F.S, k)
    out = F.U * Diagonal(d) * F.Vt
    return out
end

"""
    nuclear_norm_prox(Wgg::Mondrian, k::Number, G::Group, alpha::Vector)

Extension of `nuclear_norm_prox(W::Matrix, k::Number)` to `Mondrian` objects
"""
function nuclear_norm_prox(Wgg::Mondrian, k::Number, G::Group, alpha=nothing::Union{Vector, Nothing})

    if alpha == nothing
        alpha = ones(length(Wgg))
    end

    Pgg = Mondrian()
    for (a, Wg) in zip(alpha, Wgg)
        Pg = nuclear_norm_prox(Wg, k * a)
        push!(Pgg, Pg)
    end
    return Pgg
end


@doc raw"""

    soft_thresholding(X, τ)

Returns

```math
\begin{cases}
    x - τ &  \text{if\, } x > τ \\
    0     &  \text{if\, } x ∈ [-τ, τ] \\
    x + τ & \text{if\, } x < -τ
\end{cases}
```


Arguments
- `X`: Real, nd-Array
- `τ`: Real > 0, scalar

# Examples
```jldoctest
julia> using GNN

julia> xx = 4
4

julia> τ = 1;

julia> X = [-1 0 1; -2 1 0; 0 1 2];
julia> println(X)
[-1 0 1; -2 1 0; 0 1 2]

julia> Y = soft_thresholding(X, τ);
julia> println(Y)

```

"""
function soft_thresholding(X, τ)
    @assert τ ≥ 0
    return max.(0, X .- τ) + min.(0, X .+ τ)
end

"""
    LMO_snorm_1(Wgg::Mondrian, G::Group)

Linear minimization oracle (LMO) for the group schatten norm, `p=1`, `alpha=[1 1... 1]`
"""
function LMO_snorm_1(Wgg::Mondrian, G::Group)
    z_hat = -Inf
    local u_g_hat
    local v_g_hat
    local g_hat
    for (g, Wg) in enumerate(Wgg)
        Wg = convert(Matrix{Float64}, Wg)
        F = Arpack.svds(Wg, nsv=1, ritzvec=true)[1]
        u = F.U
        v = F.Vt'
        # z_g = u' * Wg * v
        z_g = dot(u, Wg * v)

        if z_hat < abs(z_g)
            z_hat = z_g
            u_g_hat = u
            v_g_hat = v
            g_hat = g
        end
    end
    return (u_g_hat, v_g_hat, g_hat)
end





"""
    snorm(Wgg::Mondrian; p=(1.0, 1.0)::Tuple{Number,Number}, alpha=nothing::Union{Vector, Nothing})

Schatten norm of order `p` of the matrix `m`
"""
function snorm(Wgg::Mondrian; p=(1.0, 1.0)::Tuple{Number,Number}, alpha=nothing::Union{Vector, Nothing})
    p_external, p_internal = p
    if alpha == nothing
        alpha = ones(length(Wgg))
    end

    @assert length(Wgg) == length(alpha)
    vec = []
    for (a, Wg) in zip(alpha, Wgg)
        push!(vec, snorm(Wg, p=p_internal))
    end
    out = (dot(alpha, vec.^p_external))^(1/p_external)
    return out
end

"""
    snorm(M::Matrix, p=1.0)

Schatten norm of order `p` of the matrix `m`
"""
function snorm(M::Matrix; p=1.0::Number)
    return norm(svdvals(M), p)
end

function Arpack.svds(Wgg::Mondrian; nsv::Int=1, ritzvec::Bool=true) #full::Bool = false, alg::Algorithm = default_svd_alg(A)
    Ugg = Mondrian()
    Sgg = Mondrian()
    Vtgg = Mondrian()
    for Wg in Wgg
        Wg = convert(Array{Float64}, Wg)
        F = Arpack.svds(Wg, nsv=nsv, ritzvec=ritzvec)[1]
        U = F.U
        s = F.S
        Vt = F.Vt
        push!(Ugg, U)
        push!(Sgg, Diagonal(s))
        push!(Vtgg, Vt)
    end
    return (Ugg, Sgg, Vtgg)
end


"""
    LinearAlgebra.svd(Wgg::Mondrian)

svd on each block

Returns:
3 Mondrian objects `Ugg`, `Sgg`, `Vgg` such that `Ugg*Sgg*Vgg = Wgg`.
`Sgg` contains diagonal matrices, not 1-dim arrays.
"""
function LinearAlgebra.svd(Wgg::Mondrian) #full::Bool = false, alg::Algorithm = default_svd_alg(A)
    Ugg = Mondrian()
    Sgg = Mondrian()
    Vtgg = Mondrian()
    for Wg in Wgg
        Wg = convert(Array{Float64}, Wg)
        F = LinearAlgebra.svd(Wg)
        U = F.U
        s = F.S
        Vt = F.Vt
        push!(Ugg, U)
        push!(Sgg, Diagonal(s))
        push!(Vtgg, Vt)
    end
    return (Ugg, Sgg, Vtgg)
end

"""
    LinearAlgebra.norm(Wgg::Mondrian, p=(1,1)::Tuple{Number, Number})

p-norms of internal blocks (with `p=p_internal`), forming a new vector, of wich
we compute the p-norm (`p=p_external`)
"""
function LinearAlgebra.norm(Wgg::Mondrian, p=(1,1)::Tuple{Number, Number})
    p_external, p_internal = p
    x = []
    for Wg in Wgg
        push!(x, LinearAlgebra.norm(Wg, p_internal))
    end
    out = LinearAlgebra.norm(x, p_external)
    return out
end

# function LinearAlgebra.norm(Wgg::Mondrian, p=(1,1)::Tuple{Number, Number}; alpha::Vector{Float64})
#     p_external, p_internal = p
#     vec = []
#
#     for (a, Wg) in zip(alpha, Wgg)
#         push!(vec, LinearAlgebra.norm(Wg, p=p_internal))
#     end
#
#     out = (dot(alpha, vec.^p_external))^(1.0/p_external)
#     return out
# end

function LinearAlgebra.rank(Wgg::Mondrian)
    out = Int64[]
    for Wg in Wgg
        r = LinearAlgebra.rank(Wg)
        push!(out, r)
    end
    return out
end


function squared_frobenius(Wgg::Mondrian)
    out = 0
    for Wg in Wgg
        out += sum(Wg.^2)
    end
    return out
end

println("done convex_geometry")
