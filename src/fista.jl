"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

"""
    fista(Wgg_0::Mondrian, loss::Function, grad::Function, G::Group; pars::Dict)

`f`: loss to minimize
`g`: gradient of the loss
"""
function fista(Wgg_0::Mondrian, loss::Function, grad::Function, G::Group; pars::Dict) # ;parameters

    X = Wgg_0

    debug = false

    # parameters
    tol = get(pars, "tol", 0.05)
    it_max = get(pars, "it_max", 10)
    alpha = ones(length(G)) # weights for the group norm
    erisk_max = get(pars, "erisk_max", 1e10)

    # Lipschitz constant of gradient of f
    L = get(pars, "lip_0", 0.01)
    eta = get(pars, "eta", 2)
    la = get(pars, "lambda", 5e-5)  #lambda

    Y_plus = X #y_1
    t_plus = 1

    # ts = zeros(it_max)
    # iters = zeros(it_max)

    # controls
    CONTINUE = true
    last_erisk = Inf

    # metrics
    erisk = []#-1 * ones(it_max)
    reg = -1 * ones(it_max) #regularizer
    count_backtracking = 0
    count_prox = 0

    erisk_0 = loss(X)
    reg_0 = snorm(X, p=(1.0,1.0), alpha=alpha)

    it = 1
    while CONTINUE
        print(":")
        t = t_plus
        X_minus = X
        Y = Y_plus

        if debug
            L =  0.01 #  restart always
        end

        ##backtracking
        local fX
        check_backtracking = true
        while check_backtracking

            mY = Y - (1/L)*grad(Y)
            X = nuclear_norm_prox(mY, la/L, G, alpha)
            count_prox += 1
            fX = loss(X)

            XmY = X-Y
            if fX <= loss(Y) + dot(XmY, grad(Y)) + L/2 * squared_frobenius(XmY)
                check_backtracking = false
            else
                L *= eta
                print(".")
                count_backtracking += 1
            end
        end

        t_plus = 0.5*(1 + sqrt(1 + 4*t^2))
        Y_plus = X + ((t-1)/t_plus) * (X - X_minus)

        # metrics
        # ts(it) = timing...
        # iters[it] = it
        erisk_i = fX
        reg[it] = snorm(X, p=(1,1), alpha=alpha)

        # check stop criterium
        if abs(erisk_i-last_erisk) < tol || it >= it_max || erisk_i > erisk_max
            CONTINUE = false
        else
            last_erisk = erisk_i
            push!(erisk, erisk_i)
            it += 1
        end

        # ranks = rank(X)
        # println("rank: $(ranks)")
    end

    println("")
    println("Backtracking operations: $(count_backtracking)")
    println("Prox operations: $(count_prox)")
    println("Total iterations: $(it)")

    return erisk, X
end


println("done fista")
