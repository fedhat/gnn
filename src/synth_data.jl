"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""


"""
Definition of some groups and Mondrian objects to show how the lib works
"""

if false
    n = 25

    b₁ = Block(5:10, 7:20)

    # show(b₁)
    b₂ = Block(8:15, 1:8)

    b₃ = Block(12:2:20, 16:24)

    b₄ = Block([1],1:5)
    # display(b₂)
    b_full = Block(1:n, 1:n)

    group = Group([b_full, b₁, b₂,  b₃, b₄])

    Wgg = ones(group)

    display(Wgg)

    M = assemble(Wgg, (n,n), group)

    using Plots

    plt = heatmap(reverse(M, dims=1), aspect_ratio=1, title="heatmap",
    grid=false, showaxis=false, size=(500,400),
    c=cgrad(:lightrainbow, rev=true))
    # rev== reverse the palette order
    # c==colormap name
end


function fill_affine(G::Group, d::Int, k::Int; rng)
    Wgg = Mondrian()
    for b in G
        gx, gy = size(b)
        # Wg = range(0, 2*rand(rng,1)[1]-1, length=gx) * ones(1,gy)
        #      + ones(gx,1) * range(0, 2*rand(rng,1)[1]-1, length=gy)'
        Wg = range(0, 2*rand(rng,1)[1]-1, length=gx) * ones(1,gy)
              + ones(gx,1) * range(0, rand(rng,1)[1]-1, length=gy)'
        push!(Wgg, Wg)
    end
    return assemble(Wgg, (d, k), G)
end

function fill_uniform(G::Group, d::Int, k::Int; rng)
    # @assert rng != nothing
    max_row, max_col = max_size(G)
    @assert max_row <= d
    @assert max_col <= k

    Lgg = ones(G)
    r = rand(rng, length(G)) ./ 2.0 .+ 0.5 # between [0.5, 1]
    out = assemble(r .* Lgg, (d,k), G)
    return out #full size matrix
end

function fill_full_rk1(d::Int, k::Int)
    G = Group([Block(1:d,1:k)])
    L = rand(d, 1)*rand(1, k)
    return L, G #full size matrix
end

function fill_small_rectangles_flat(d::Int, k::Int; rng)

    G = Group([Block(1:d,1:k),
               Block(1:2*floor(d/3), 1:floor(k/3)),
               Block(floor(d/3):d, 1:2*floor(k/3))
              ])

    L = fill_uniform(G, d, k, rng=rng)

    return L, G
end

function append_sparse_blocks!(G::Group, num_groups::Number, d::Number, k::Number, width::Number, height::Number, percentage=0.7::Number; rng)

    for i in 1:num_groups
        @assert width <= k
        @assert height <= d
        r1 = 1 + floor(rand(rng, 1)[1] * (d - height))
        r2 = r1 + height - 1;
        c1 = 1 + floor.(rand(rng, 1)[1] * (k - width))
        c2 = c1 + width - 1;
        rows = randsubseq(rng, r1:r2, percentage)
        cols = randsubseq(rng, c1:c2, percentage)
        new_block = Block(rows, cols)
        push!(G, new_block)
    end
end

println("done synth_data")
