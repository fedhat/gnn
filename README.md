# GNN

Convex optimization with Group Schatten Norm regularization.
This is a Julia implementation of algebric structures, optimization algorithms, and numerical experiments presented in [Pierucci 2017](https://hal.inria.fr/tel-01572186/ "pdf"), 2nd chapter.

![alt text](docs/src/figures1/plot_observations.png)
![alt text](docs/src/figures1/plot_W_sol.png)
![alt text](docs/src/figures1/plot_A_star.png)

![alt text](docs/src/figures2/plot_observations.png)
![alt text](docs/src/figures2/plot_W_sol.png)
![alt text](docs/src/figures2/plot_A_star.png)

# Requirements
Install [Julia 1.4](https://julialang.org/) or later.

Install depandancies:
```
$ julia
julia> ]
(@v1.4) pkg> add Arpack Debugger LinearAlgebra Plots Random Statistics Test
```


# Install
Clone this into `PATH`.

```
$ julia
julia> ]
(@v1.4) pkg> dev PATH/gnn
(@v1.4) pkg> activate GNN
```

# Run
```
julia examples/start.jl
```

# Cite
If you use this, please cite :

```
@phdthesis{pierucci:tel-01572186,
  TITLE = {{Nonsmooth Optimization for Statistical Learning with Structured Matrix Regularization}},
  AUTHOR = {Pierucci, Federico},
  URL = {https://hal.inria.fr/tel-01572186},
  SCHOOL = {{Universit{\'e} Grenoble-Alpes}},
  YEAR = {2017},
  MONTH = Jun,
  KEYWORDS = {first-order optimization ; conditional gradient ; smoothing ; nuclear-norm ; machine learning ; mathematical optimization ; methodes de premier ordre ; gradient conditionnel ; lissage ; norme nucleaire ; apprentissage automatique ; optimisation mathematique},
  TYPE = {Theses},
  PDF = {https://hal.inria.fr/tel-01572186/file/main_manuscript.pdf},
  HAL_ID = {tel-01572186},
}
```

# License
This file is part of software distributed under the [CeCILL](https://cecill.info) free software license.
