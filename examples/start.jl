"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

using Plots, Statistics, LinearAlgebra, Random
using Debugger


using GNN

function generate_synthetic_data(name="affine"::String; synth_parameters)
    # generate synthetic data and normalize
    if name == "nine"
        rng = MersenneTwister(33)
        d = 410 # rows
        k = 500 # columns

        # L, G = fill_full_rk1(d, k)
        L, G = fill_small_rectangles_flat(d, k, rng=rng)
        L .-= mean(L)
        L ./= std(L)
        A_star = L

    elseif name == "flat"
        rng = MersenneTwister(34)
        d = synth_parameters["d"] # rows
        k = synth_parameters["k"] # columns
        width = synth_parameters["width"]
        height = synth_parameters["height"]
        n_blocks = 35
        # rng = MersenneTwister(45)

        sa_G = Group([])
        append_small_blocks!(sa_G, n_blocks, d, k, width, height, rng)
        sa_L = fill_uniform(sa_G, d, k, rng=rng)

        a_G = Group([])
        append_group_crossed!(a_G, d, k)
        a_L = fill_uniform(a_G, d, k, rng=rng)

        L = a_L + sa_L
        L .-= mean(L)
        L ./= std(L)
        A_star = L
        G = [a_G; sa_G]


    elseif name == "affine"
        rng = MersenneTwister(34)
        d = synth_parameters["d"] # rows
        k = synth_parameters["k"] # columns
        width = synth_parameters["width"]
        height = synth_parameters["height"]
        n_blocks = 25
        # rng = MersenneTwister(45)

        sa_G = Group([])
        append_small_blocks!(sa_G, n_blocks, d, k, width, height, rng)
        sa_L = fill_affine(sa_G, d, k, rng=rng)

        a_G = Group([])
        append_group_crossed!(a_G, d, k)
        a_L = fill_affine(a_G, d, k, rng=rng)

        L = a_L + sa_L
        L .-= mean(L)
        L ./= std(L)
        A_star = L
        G = [a_G; sa_G]


    elseif name == "sparse"
        rng = MersenneTwister(34)
        d = synth_parameters["d"] # rows
        k = synth_parameters["k"] # columns
        width = synth_parameters["width"]
        height = synth_parameters["height"]
        # rng = MersenneTwister(45)
        n_blocks = 1
        sp_G = Group([])
        append_sparse_blocks!(sp_G, n_blocks, d, k, 2*width, 3*height, rng=rng)
        sp_L = fill_affine(sp_G, d, k, rng=rng)

        n_blocks = 25
        sa_G = Group([])
        append_small_blocks!(sa_G, n_blocks, d, k, width, height, rng)
        sa_L = fill_affine(sa_G, d, k, rng=rng)


        a_G = Group([])
        append_group_crossed!(a_G, d, k)
        a_L = fill_affine(a_G, d, k, rng=rng)

        L = a_L + sa_L + sp_L
        L .-= mean(L)
        L ./= std(L)
        A_star = L
        G = [a_G; sa_G; sp_G]
    else
        throw(ArgumentError("misspelling"))
    end
    return A_star, G
end


function launch_fista(A_star, G, out_folder="outputs"; data_params, optimization_params)
    rng = MersenneTwister(33)

    if !isdir(out_folder)
        mkdir(out_folder)
    end

    dims = size(A_star)
    d, k = dims
    palette = :lightrainbow
    # palette = :imola
    colorbar_limits = (minimum(A_star), maximum(A_star))
    plt = heatmap(reverse(A_star, dims=1), aspect_ratio=1,
                  title="Ground Truth",
                  grid=false, showaxis=false, size=(500,400),
                  c=cgrad(palette, rev=true), clims=colorbar_limits)
    savefig(plt, joinpath(out_folder,"plot_A_star.png"))

    idx = randsubseq(rng, collect(1:(d*k)), data_params["perc"])

    ## define loss and gradient of the loss (regularization excluded)
    MI(W) = W[idx]
    MI_adj(x) = mask_idx_adj(x, idx, dims)

    Aa(Wgg) = assemble(Wgg, dims, G)
    Aa_adj(W) = op_adjoint(W, G)

    N = length(idx)
    grad(Wgg::Mondrian) = Aa_adj(MI_adj(1.0 / N * (MI(Aa(Wgg)) - X )))
    loss(Wgg::Mondrian) = 0.5 / N * norm(MI(Aa(Wgg))-X, 2)^2

    ## sampling from data
    X = A_star[idx] +  data_params["noise_sigma"] .* randn(length(idx))

    plt = heatmap(reverse(MI_adj(X), dims=1), aspect_ratio=1,
                  title="Observed Data",
                  grid=false, showaxis=false, size=(500,400),
                  c=cgrad(palette, rev=true), clims=colorbar_limits)
    savefig(plt, joinpath(out_folder, "plot_observations.png"))

    Wgg_0 = zeros(G) # Starting iterate for initialization
    ######################################################################
    erisk, Wgg_sol = fista(Wgg_0, loss, grad, G, pars=optimization_params)
    ######################################################################
    # print("erisk: ")
    # println(erisk)

    plt3 = plot(1:length(erisk), erisk, title="Empirical Risk")
    savefig(plt3, joinpath(out_folder, "plot_erisk.png"))

    W_sol = assemble(Wgg_sol, dims, G)
    plt4 = heatmap(reverse(W_sol, dims=1), aspect_ratio=1,
    title="Retrieved Information",
    grid=false, showaxis=false, size=(500,400),
    c=cgrad(palette, rev=true), clims=colorbar_limits)
    savefig(plt4, joinpath(out_folder, "plot_W_sol.png"))

    ranks = rank(Wgg_sol)
    println("rank: $(ranks)")
    max_rank = minimum.(sizes(Wgg_sol))
    println("max rank: $(max_rank)")
    normalized_rank = ranks ./ max_rank
    println("normalized rank: $(normalized_rank)")
end

#######################################################################
#
# name = "nine"
# op = Dict("lip_0" => 1e-6, #10^-5 for size 500x500
#           "tol" => 1e-6,
#           "lambda" => 1e-4,
#           "it_max" => 500)

name = "sparse"
op = Dict("lip_0" => 1e-6, #10^-5 for size 500x500
          "tol" => 1e-8,
          "lambda" => 1e-5,
          "it_max" => 500)

dp = Dict("noise_sigma" => 0.2,
          "perc" => 0.10) #percentage of observations

sp = Dict("d" => 1000,
          "k" => 1100,
          "width" => 220,
          "height" => 200)

ground_truth, G = generate_synthetic_data(name, synth_parameters=sp)

launch_fista(ground_truth, G, optimization_params=op, data_params=dp)
