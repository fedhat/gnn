"""
  Federico Pierucci, 2020

  This file is part of software distributed under
  the CeCILL free software license.

"""

using Plots, Statistics, LinearAlgebra, Random
using GNN



d = 40 # rows
k = 60 # columns
width = 10
height = 20
rng = MersenneTwister(45)

sa_G = Group([])
append_sparse_block!(sa_G, 5, d, k, width, height, rng=rng)
sa_L = fill_uniform(sa_G, d, k, rng=rng)

a_G = Group([])
append_group_crossed!(a_G, d, k)
a_L = fill_uniform(a_G, d, k, rng=rng)

aa_G = Group([])
append_small_blocks!(sa_G, 5, d, k, width, height, rng)
aa_L = fill_affine(aa_G, d, k, rng=rng)

L = a_L + sa_L
G = [a_G; sa_G]

println(G)
